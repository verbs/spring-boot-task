package appTest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

public class Main {
  public static void main(String[] args) throws Exception{
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn = null;
    Properties info = new Properties();
    //info.put("proxy_type", "4"); // SSL Tunneling
    info.put("proxy_host", "134.192.110.179");
    info.put("proxy_port", "4567");
//    info.put("proxy_user", "[proxy user]");
//    info.put("proxy_password", "[proxy password]");
    info.put("user", "root");
    info.put("password", "orcl");
//    String url = "jdbc:mysql://172.168.27.118:3306/test?user=root&password=cattsoft&useUnicode=true&characterEncoding=utf8&autoReconnect=true&useSSL=false&serverTimezone=UTC";
//    conn = DriverManager.getConnection(url);
    conn = DriverManager.getConnection("jdbc:mysql://106.53.14.84:3306/mysql", info);
//    conn = DriverManager.getConnection(url, info);


    Statement stmt = conn.createStatement();
//    ResultSet rs = stmt.executeQuery("Select NOW()");
    ResultSet rs = stmt.executeQuery("Select NOW()");
    rs.next();
    System.out.println("Data- " + rs.getString(1));
    rs.close();
    stmt.close();
    conn.close();
  }
}