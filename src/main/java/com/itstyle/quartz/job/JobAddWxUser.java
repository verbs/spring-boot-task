package com.itstyle.quartz.job;

import com.itstyle.quartz.util.WxUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;


/**
 * 更新微信用户的UID到数据库表中 T_WX_USER
 */
@DisallowConcurrentExecution
public class JobAddWxUser implements  Job,Serializable {

    private static final Logger logger = LoggerFactory.getLogger(JobAddWxUser.class);
    private static final long serialVersionUID = 1L;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        WxUtil.setWxUid(this.getClass().getSimpleName());
    }

    public static void main(String[] args){
//        WxUtil.setWxUid();
    }
}
