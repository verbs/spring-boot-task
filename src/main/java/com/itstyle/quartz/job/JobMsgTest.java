package com.itstyle.quartz.job;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.itstyle.quartz.util.WxUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.itstyle.quartz.service.IJobService;
/**
 * 实现序列化接口、防止重启应用出现quartz Couldn't retrieve job because a required class was not found 的问题
 */
public class JobMsgTest implements  Job,Serializable {
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		WxUtil.postWxMsg("这是一条测试信息！",this.getClass().getSimpleName());
	}
}
