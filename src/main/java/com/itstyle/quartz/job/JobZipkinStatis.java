package com.itstyle.quartz.job;

import com.itstyle.quartz.util.WxUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@DisallowConcurrentExecution
public class JobZipkinStatis implements  Job,Serializable {
    
	private static final Logger logger = LoggerFactory.getLogger(JobZipkinStatis.class);
	private static final long serialVersionUID = 1L;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {

		List<String> list =new ArrayList();
		list.add("{?=call GET_ZIPKIN_COUNT()}");

		String[] arrFunc = new String[list.size()];
		list.toArray(arrFunc);
		WxUtil.postOraclFunc(arrFunc,this.getClass().getSimpleName(),"IPIS");

	}


}
