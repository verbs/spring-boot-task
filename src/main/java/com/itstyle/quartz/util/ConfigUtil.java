package com.itstyle.quartz.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigUtil {
    //资源文件
    public static Properties pros = null;

    static {
        pros = new Properties();
        try {
            //加载资源文件
            InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.properties");
            if(in == null) {
                throw new FileNotFoundException("配置文件未找到");
            }
            pros.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
