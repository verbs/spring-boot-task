package com.itstyle.quartz.util;

import com.itstyle.quartz.util.ConfigUtil;

import java.sql.*;
import java.util.*;

/**
 * @Author: LiGe
 * @Date: 2018/10/21 11:50
 * @description: Oracle连接工具类
 */
public class DBUtil {

     private Connection conn=null;
    //创建预编译对象
    private PreparedStatement ps=null;
    //创建结果集
    private ResultSet rs = null;

    private CallableStatement cs = null;


    /*获取数据库连接 */
    public  Connection getCon(String DBName){
        try {
            Class.forName(ConfigUtil.pros.getProperty("oracleDriver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        conn = null;
        try {
            if(DBName==null) {
                conn = DriverManager.getConnection(ConfigUtil.pros.getProperty("oracleURL"),
                        ConfigUtil.pros.getProperty("oracleUser"),
                        ConfigUtil.pros.getProperty("oraclePwd"));
            }else if("IPIS".equals(DBName)){
                conn = DriverManager.getConnection(ConfigUtil.pros.getProperty("oracleURL_IPIS"),
                        ConfigUtil.pros.getProperty("oracleUser_IPIS"),
                        ConfigUtil.pros.getProperty("oraclePwd_IPIS"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
 
    /*关闭数据库连接*/
    public void dbClose() throws SQLException {
        if (rs != null) rs.close();
        if (ps != null) ps.close();
        if (cs != null) cs.close();
        if (conn != null) conn.close();
    }
 

    /*插入*/
    public int execSql(String sql, String DBName) throws SQLException {
        int result = 0;
        conn = getCon(DBName);
//        String sql = "insert into student values(?,?,?)";
        try {
            ps = conn.prepareStatement(sql);
//            ps.setInt(1,2);
//            ps.setString(2,"老王");
//            ps.setString(3,"女");
            result = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
             dbClose();
        }
        return result;
    }


    public List resultSetToList(ResultSet rs) throws java.sql.SQLException {
        if (rs == null)
            return Collections.EMPTY_LIST;
        ResultSetMetaData md = rs.getMetaData(); //得到结果集(rs)的结构信息，比如字段数、字段名等
        int columnCount = md.getColumnCount(); //返回此 ResultSet 对象中的列数
        List list = new ArrayList();
        while (rs.next()) {
            Map rowData = new HashMap(columnCount);
            for (int i = 1; i <= columnCount; i++) {
                rowData.put(md.getColumnName(i), rs.getObject(i));
            }
            list.add(rowData);
        }
        return list;
    }


    /*查询*/
    public List querySql(String sql,String DBName) throws SQLException {
        List result = null;
        conn = getCon(DBName);
        try {
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            result = resultSetToList(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            dbClose();
        }
        return result;
    }

    public String execFunc(String sql,String DBName) throws SQLException {
        String result = "";
        conn = getCon(DBName);
        try {
            cs=conn.prepareCall(sql);
            cs.registerOutParameter(1, Types.VARCHAR);
            //cs.setString(2,"");
            if (!cs.execute()) {
                result = cs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            dbClose();
        }
        return result;
    }
}
