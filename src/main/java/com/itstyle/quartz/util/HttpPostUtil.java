package com.itstyle.quartz.util;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * @author yunfeng
 * @version V1.0
 * @date 2018/5/3 16:33
 */
public class HttpPostUtil {

    private final static Logger log = LoggerFactory.getLogger(HttpPostUtil.class);

    /**
     * @param url
     * @return
     */
    public static String getData(String url) {
        HttpClient client = new HttpClient();

        setHttpProxy(client);

//        if(ConfigUtil.pros.getProperty("proxyHost")!=null) {
//            client.getHostConfiguration().setProxy(ConfigUtil.pros.getProperty("proxyHost"),
//                    Integer.parseInt(ConfigUtil.pros.getProperty("proxyPort")));
//        }


        GetMethod method = new GetMethod(url);
        log.info("reqUrl: " + url);
        String respStr = "";
        try {
            int statusCode = client.executeMethod(method);
            log.info("Status Code = " + statusCode);
            respStr = method.getResponseBodyAsString();
            method.releaseConnection();
        } catch (IOException e) {
            log.error("发送HTTP GET请求失败！详情：" + e.toString());
            e.printStackTrace();
        }
        return respStr;
    }


    /**
     * 添加代理属性，使客户端通过代理访问网络
     * @param client
     */
    public static void setHttpProxy(HttpClient client){
        if(ConfigUtil.pros.getProperty("proxyHost")!=null) {
            client.getHostConfiguration().setProxy(ConfigUtil.pros.getProperty("proxyHost"),
                    Integer.parseInt(ConfigUtil.pros.getProperty("proxyPort")));
        }
    }


    /**
     * @param url
     * @param jsonStr
     * @return
     */
    public static String postJson(String url, String jsonStr) {
        HttpClient client = new HttpClient();
        setHttpProxy(client);
//        //代理
//        if(ConfigUtil.pros.getProperty("proxyHost")!=null) {
//            client.getHostConfiguration().setProxy(ConfigUtil.pros.getProperty("proxyHost"),
//                    Integer.parseInt(ConfigUtil.pros.getProperty("proxyPort")));
//        }

        PostMethod method = new PostMethod(url);
        method.setRequestHeader("Content-Type", "application/json;charset=utf-8");
        String postBody = buildPostData(jsonStr);
        log.info("reqUrl: " + url);
        log.info("postBody:\r\n" + postBody);
        method.setRequestBody(postBody);
        String respStr = "";
        try {
            int statusCode = client.executeMethod(method);
            log.info("Status Code = " + statusCode);
            respStr = method.getResponseBodyAsString();
//            log.info("respStr:" + respStr);
            method.releaseConnection();
        } catch (IOException e) {
            log.error("发送HTTP POST JSON请求失败！详情：" + e.toString());
            e.printStackTrace();
        }
        return respStr;
    }

    /**
     * @param url
     * @param jsonStr
     * @return
     */
    public static String postJsonHeader(String url, Map<String, String> headerMap, String jsonStr) {
        HttpClient client = new HttpClient();
        setHttpProxy(client);
        PostMethod method = new PostMethod(url);
        method.setRequestHeader("Content-Type", "application/json;charset=utf-8");
        for (Map.Entry<String, String> entry : headerMap.entrySet()) {
            String key = entry.getKey();
            String val = entry.getValue();
            method.setRequestHeader(key, val);
        }
        String postBody = buildPostData(jsonStr);
        log.info("HTTP　ReqUrl: " + url);
        log.info("HTTP　ReqContent: Content-Type: application/json;charset=utf-8");
        log.info("HTTP PostBody:\r\n" + postBody);
        method.setRequestBody(postBody);
        String respStr = "";
        try {
            int statusCode = client.executeMethod(method);
            respStr = method.getResponseBodyAsString();
            if (statusCode == HttpStatus.SC_MOVED_PERMANENTLY || statusCode == HttpStatus.SC_MOVED_TEMPORARILY) {
                Header header = method.getResponseHeader("Location");
                String location = "";
                if (header != null) {
                    location = header.getValue();
                    method.setURI(new URI(location));
                    statusCode = client.executeMethod(method);
                    respStr = method.getResponseBodyAsString();
                }

            }
            log.info("Status Code = " + statusCode);
//            log.info("respStr:" + respStr);
            method.releaseConnection();
        } catch (IOException e) {
            String detail = "发送HTTP POST JSON请求失败！详情：" + e.toString();
            log.error(detail, e);
            return null;
        }
        return respStr;
    }


    /**
     * @param url     url
     * @param formStr form参数
     * @return 请求返回
     */
    public static String postForm(String url, String formStr) {
        log.info("post URL：" + url);
        HttpClient client = new HttpClient();
        setHttpProxy(client);
        PostMethod method = new PostMethod(url);
        method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        String postBody = buildPostData(formStr);
        log.info("postBody:\n" + postBody);
        method.setRequestBody(postBody);
        int statusCode = 0;
        String respStr = "";
        try {
            statusCode = client.executeMethod(method);
            log.info("http return status code = " + statusCode);
            respStr = method.getResponseBodyAsString();
//            log.info("http return respStr = " + respStr);
        } catch (IOException e) {
            log.error("发送http form请求失败！详情：" + e.toString());
            e.printStackTrace();
        }
        method.releaseConnection();
        return respStr;
    }


    /**
     * @param formStr form参数
     * @return
     */
    private static String buildPostData(String formStr) {
        StringBuilder sb = new StringBuilder();
        sb.append(formStr);
        sb.append("\r\n");
        sb.append("\r\n");
        return sb.toString();
    }

    public static void main(String[] args) {

//        if(ConfigUtil.pros.getProperty("proxyHost")!=null) {
//            System.out.println(ConfigUtil.pros.getProperty("proxyHost"));
//        }else{
//            System.out.println("无");
//        }


//        String jsonStr = "test";
//        postJson("http://wxpusher.zjiecode.com/api/send/message",jsonStr);

//        String jsonStr = getData("http://wxpusher.zjiecode.com/api/fun/wxuser?appToken=AT_ftwyWUsD8y8YlNRvrIjjeg2gwNmvEQU8");
//        WxRes wxRes = JSON.toJavaObject(JSON.parseObject(jsonStr), WxRes.class);
//        JSONObject a = (JSONObject)JSON.toJSON(wxRes.getData());
//        JSONArray jsonArr = a.getJSONArray("records");
//        List<Record> lists = jsonArr.toJavaList(Record.class);
//
//        for (Record list : lists) {
//            System.out.println(list.getNickName()+": "+list.getUid());
//
//        }






    }

}
