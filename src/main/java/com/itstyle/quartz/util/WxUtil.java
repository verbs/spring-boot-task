package com.itstyle.quartz.util;

import bean.Record;
import bean.WxRes;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.itstyle.quartz.util.HttpPostUtil.postJson;

public class WxUtil {

    /**
     * 执行Oracle自定义函数
     * @param arrFuncs 函数名称
     */
    public static void postOraclFunc(String[] arrFuncs, String className, String DBName){
        String wxMsg = "";
        String[] arrUids = getWxUid(className);
        if(arrUids!=null){
            try {
                DBUtil dbUtil = new DBUtil();
                if(arrFuncs.length>0){
                    for (String arrFunc : arrFuncs) {
                        wxMsg = wxMsg + dbUtil.execFunc(arrFunc, DBName);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            postWxMsg(arrUids,wxMsg);
;        }

    }

    /**
     * 发送消息
     * @param wxMsg
     * @param className
     */
    public static void postWxMsg(String wxMsg,String className){
        String[] arrUids = getWxUid(className);
        postWxMsg(arrUids,wxMsg);
    }

    /**
     * 发送微信消息到指定用户
     * @param arrUids 用户的usid
     * @param wxMsg 消息内容
     */
    public static void postWxMsg(String[] arrUids,String wxMsg){

        if(arrUids!=null && wxMsg!=null){
            try {
                Map map = new HashMap();
                map.put("appToken", ConfigUtil.pros.getProperty("wxAppToken"));
                map.put("uids",arrUids);
                map.put("contentType",3); //内容类型 1：文字，2：html，3：markdown
                map.put("content",wxMsg);

                String jsonStr = JSON.toJSONString(map);
                postJson(ConfigUtil.pros.getProperty("wxSendUrl"),jsonStr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 获取用户UID
     * @return
     */
    public static String[] getWxUid(String className){
        String uid =null;
        String iGroup = "";
        String sql = "";
        try {
            DBUtil dbUtil = new DBUtil();
            sql = "select S_UID from T_WX_USER where I_IS_ACTIVE=1";
            sql = sql+" and S_CLASS_NAME like '%"+className+"%'";

            List list = dbUtil.querySql(sql, null);
            Iterator it = list.iterator();
            while(it.hasNext()) {
                Map hm = (Map)it.next();
                if("".equals(uid)){
                    uid = hm.get("S_UID").toString();
                }else{
                    uid = uid + "," + hm.get("S_UID").toString();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(uid!=null){
            return uid.split(",");
        }else{
            return null;
        }

    }

    /**
     * 更新库表中的用户UID
     */
    public static void setWxUid(String className){
        String jsonStr = HttpPostUtil.getData(ConfigUtil.pros.getProperty("wxUserUrl")
                +ConfigUtil.pros.getProperty("wxAppToken"));

        WxRes wxRes = JSON.toJavaObject(JSON.parseObject(jsonStr), WxRes.class);
        JSONObject a = (JSONObject)JSON.toJSON(wxRes.getData());
        JSONArray jsonArr = a.getJSONArray("records");
        List<Record> lists = jsonArr.toJavaList(Record.class);

        DBUtil dbUtil = new DBUtil();
        StringBuffer stringBuffer = new StringBuffer();
        String message = null;
        for (Record list : lists) {
            try {
                String id = list.getId();
                String name = list.getNickName();
                String uid = list.getUid();

                stringBuffer.setLength(0);
                stringBuffer.append("select S_UID from T_WX_USER where S_UID=");
                stringBuffer.append("'").append(uid).append("'");

                List li = dbUtil.querySql(stringBuffer.toString(), null);
                if(li!=null){
                    if(li.size()==0){
                        stringBuffer.setLength(0);
                        stringBuffer.append("insert into T_WX_USER(S_WX_NAME,S_UID) values(");
                        stringBuffer.append("'").append(name).append("'");
                        stringBuffer.append(",'").append(uid).append("'");
                        stringBuffer.append(")");
                        dbUtil.execSql(stringBuffer.toString(), null);
                        if(message==null){
                            message = "【"+name+"】";
                        }else{
                            message = message+",【"+name+"】";
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(message!=null) {
            message = "添加用户："+message+" 成功！";
            postWxMsg(message, className);
        }
    }

}
